#ifndef APPLICATION_HPP_
#define APPLICATION_HPP_

#include "QApplication"

namespace gui
{

class Application : QApplication
{
public:
  Application(int argc, char* argv[]) : QApplication(argc, argv) {}
  int run();
};

} // namespace gui

#endif /* APPLICATION_HPP_ */
