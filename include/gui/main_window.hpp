#ifndef MAIN_WINDOW_HPP_
#define MAIN_WINDOW_HPP_

#include <memory>

#include "QMainWindow"
#include "QTimer"
#include "ui_main_window.h"

class MainWindow : public QMainWindow, private Ui_MainWindow
{
public:
  MainWindow(QWidget* parent = nullptr);

private slots:
  void onControlButtonClick();
  void onTimerTick();

private:
  enum class CurrentMode
  {
    COUNT_DOWN,
    NOT_WORKING
  };

  void setup();
  void stopCountDown();
  void startCountDown();
  void updateTimeLeftLabel();

  QTimer* m_secTimer;
  CurrentMode m_mode;
  int64_t m_intervalInSeconds;
  int64_t m_secondsLeft;
};

#endif /* MAIN_WINDOW_HPP_ */
