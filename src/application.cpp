#include "gui/application.hpp"
#include "gui/main_window.hpp"

#include <iostream>
#include <memory>

namespace gui {

int Application::run()
{
  std::unique_ptr<MainWindow> mainWindow{new MainWindow()};
  mainWindow->show();

  return exec();
}
} // namespace gui
